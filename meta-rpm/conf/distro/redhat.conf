DISTRO = "redhat"
DISTRO_NAME = "redhat distro"
DISTRO_VERSION = "centos8-stream"
DISTRO_CODENAME = "centos8-stream"
SDK_VENDOR = "-redhatsdk"
SDK_VERSION = "${@d.getVar('DISTRO_VERSION').replace('snapshot-${DATE}', 'snapshot')}"

MAINTAINER = "meta-rpm team"

# We avoid packages owning files because it conflicts with those owned by rpms
# In particular i had issues with /usr/bin owned by filesystem
DIRFILES = ""

TARGET_VENDOR = "-redhat"

LOCALCONF_VERSION = "1"

TCMODE = "redhat"

INIT_MANAGER = "systemd"

DISTRO_VERSION[vardepsexclude] = "DATE"
SDK_VERSION[vardepsexclude] = "DATE"

# We need this because chrpath doesn't allow creating rpaths if none exists
# And we can't bootstrap it, so require it on the host
HOSTTOOLS += " patchelf newuidmap newgidmap"

# Override these in redhat based distros
REDHAT_DEFAULT_DISTRO_FEATURES = "systemd alsa ldconfig largefile ptest multiarch usrmerge acl bluetooth ext2 ipv4 ipv6 wifi xattr nfs pci x11 vfat wayland"
REDHAT_DEFAULT_EXTRA_RDEPENDS = "packagegroup-core-boot"
REDHAT_DEFAULT_EXTRA_RRECOMMENDS = ""

DISTRO_FEATURES ?= "${DISTRO_FEATURES_DEFAULT} ${REDHAT_DEFAULT_DISTRO_FEATURES}"

USER_CLASSES = "buildstats"

PREFERRED_VERSION_linux-yocto ?= "5.8%"
PREFERRED_VERSION_linux-yocto-rt ?= "5.4%"

# Work around a missing kmod dependency (for postinst) in the poky kernel recipe
RDEPENDS_${KERNEL_PACKAGE_NAME}-base_append = " kmod"

#PREFERRED_PROVIDER_virtual/kernel ?= "linux-yocto"
PREFERRED_PROVIDER_virtual/libc = "glibc"
PREFERRED_PROVIDER_virtual/libc-locale = "glibc"
PREFERRED_PROVIDER_linux-libc-headers = "kernel-headers"
PREFERRED_PROVIDER_gzip-native = "gzip-native"
PREFERRED_PROVIDER_pkgconfig-native = "pkgconf-native"
PREFERRED_PROVIDER_udev = "systemd-udev"

PREFERRED_RPROVIDER_python3 = "python36"
PREFERRED_RPROVIDER_python3-dev = "python36"

PREFERRED_PROVIDER_virtual/update-alternatives = "chkconfig"
PREFERRED_PROVIDER_virtual/update-alternatives-native = "chkconfig-native"
VIRTUAL-RUNTIME_update-alternatives = "chkconfig"

SDK_NAME = "${DISTRO}-${TCLIBC}-${SDKMACHINE}-${IMAGE_BASENAME}-${TUNE_PKGARCH}-${MACHINE}"
SDKPATH = "/opt/${DISTRO}/${SDK_VERSION}"

DISTRO_EXTRA_RDEPENDS += " ${REDHAT_DEFAULT_EXTRA_RDEPENDS}"
DISTRO_EXTRA_RRECOMMENDS += " ${REDHAT_DEFAULT_EXTRA_RRECOMMENDS}"

TCLIBCAPPEND = ""

# Use the gcc from the rpms, as we need it to produce code that links
# to the libstdc++ from the rpms.
NON_RPMBASED_DEPENDS ?= " gcc-native binutils-native gcc-runtime-native"
DEPENDS_append_class-cross = "${NON_RPMBASED_DEPENDS}"
DEPENDS_append_class-native = "${NON_RPMBASED_DEPENDS}"
# gcc-native actually needs the real libgcc-native, so drop this from ASSUME_PROVIDES
ASSUME_PROVIDED_remove = "libgcc-native"

# Remove hosttools we don't require anymore because we use gcc-native & binutils-native
# This is not strictly needed, but means we don't accidentally mix host and -native tools
# Note: We can't remove gcc and g++ as they are checked by sanity.bbclass
HOSTTOOLS_remove = "ar as nm objcopy ranlib readelf"
HOSTTOOLS_NONFATAL_remove = "gcc-ar ld.bfd ld.gold"

PREMIRRORS ??= "\
bzr://.*/.*   http://downloads.yoctoproject.org/mirror/sources/ \n \
cvs://.*/.*   http://downloads.yoctoproject.org/mirror/sources/ \n \
git://.*/.*   http://downloads.yoctoproject.org/mirror/sources/ \n \
gitsm://.*/.* http://downloads.yoctoproject.org/mirror/sources/ \n \
hg://.*/.*    http://downloads.yoctoproject.org/mirror/sources/ \n \
osc://.*/.*   http://downloads.yoctoproject.org/mirror/sources/ \n \
p4://.*/.*    http://downloads.yoctoproject.org/mirror/sources/ \n \
svn://.*/.*   http://downloads.yoctoproject.org/mirror/sources/ \n"

SANITY_TESTED_DISTROS ?= " \
            fedora-32 \n \
            fedora-33 \n \
            "
# QA check settings - a little stricter than the OE-Core defaults
# (none currently necessary as we now match OE-Core)
#WARN_TO_ERROR_QA = "X"
#WARN_QA_remove = "${WARN_TO_ERROR_QA}"
#ERROR_QA_append = " ${WARN_TO_ERROR_QA}"

require conf/distro/include/no-static-libs.inc
require conf/distro/include/security_flags.inc

INHERIT += "reproducible_build"

# We use redhat rpms for core -native packages too, and they use a lib64 baselib, so override that
BASELIB = "lib64"
libdir_native = "${prefix_native}/lib64"
base_libdir_native = "/lib64"
libdir_nativesdk = "${prefix_nativesdk}/lib64"
base_libdir_nativesdk = "/lib64"

BB_SIGNATURE_HANDLER ?= "OEEquivHash"
BB_HASHSERVE ??= "auto"

FAKEROOTBASEENV = "PSEUDO_BINDIR=${PSEUDO_SYSROOT}${bindir_native} PSEUDO_LIBDIR=${PSEUDO_SYSROOT}${prefix_native}/lib64/pseudo/lib PSEUDO_PREFIX=${PSEUDO_SYSROOT}${prefix_native} PSEUDO_IGNORE_PATHS=${PSEUDO_IGNORE_PATHS} PSEUDO_DISABLED=1"

LIBC_DEPENDENCIES = "glibc \
                     glibc-devel \
                     glibc-utils \
                     "

EXTRA_AUTORECONF_remove = "--exclude=autopoint --exclude=libtoolize"

TOOLCHAIN_HOST_TASK = ""
