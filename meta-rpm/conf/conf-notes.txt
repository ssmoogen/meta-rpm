
### Shell environment set up for builds. ###

You can now run 'bitbake <target>'

Common targets are:
    rpmbased-minimal-image
    rpmbased-test-image
    rpmbased-desktop-image

You can also run generated qemu images with a command like 'runqemu qemux86'

Other commonly useful commands are:
 - 'devtool' and 'recipetool' handle common recipe tasks
 - 'bitbake-layers' handles common layer tasks
 - 'oe-pkgdata-util' handles common target package tasks
