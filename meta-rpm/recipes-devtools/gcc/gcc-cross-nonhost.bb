SUMMARY = "GNU cc and gcc C compilers"
HOMEPAGE = "http://www.gnu.org/software/gcc/"
SECTION = "devel"

LICENSE = "GPL-3.0-with-GCC-exception & GPLv3"
LIC_FILES_CHKSUM = "\
    file://COPYING;md5=59530bdf33659b29e73d4adb9f9f6552 \
    file://COPYING3;md5=d32239bcb673463ab874e80d47fae504 \
    file://COPYING3.LIB;md5=6a6a8e020838b23406c81b19c1d46df6 \
    file://COPYING.LIB;md5=2d5025d4aa3495befef8f17206a5b0a1 \
    file://COPYING.RUNTIME;md5=fe60d87048567d4fe8c8a0ed2448bcc8 \
"

def cross_suffix(d):
    build_arch = d.getVar("BUILD_ARCH")
    target_arch = d.getVar("TARGET_ARCH")
    if build_arch != target_arch:
      return "-cross-%s" % target_arch
    else:
      return "-disabled-cross-%s" % target_arch

PN = "gcc${@cross_suffix(d)}"
BPN = "gcc"

# This is based on gcc-8.3.1-5 from centos:
PV = "8.3.1"
PR = "5"

BINV = "8"

SRCDATE = "20191121"
SRCNAME = "${BPN}-${PV}-${SRCDATE}"

INHIBIT_DEFAULT_DEPS = "1"
NATIVEDEPS = "mpfr-native gmp-native libmpc-native zlib-native flex-native glibc systemtap-sdt-devel kernel-headers"
DEPENDS = "virtual/${TARGET_PREFIX}binutils ${NATIVEDEPS}"

PROVIDES = "virtual/${TARGET_PREFIX}gcc virtual/${TARGET_PREFIX}g++"

S = "${WORKDIR}/${SRCNAME}"
B = "${WORKDIR}/${SRCNAME}/build.${HOST_SYS}.${TARGET_SYS}"

inherit autotools gettext texinfo cross

SRC_URI = " \
        https://src.fedoraproject.org/repo/pkgs/gcc/gcc-8.3.1-20191121.tar.xz/sha512/3a602cd5fcbd36c13cbde54545975716442d96d57c8a2bb107e04a3a4c61170544d924fe72c56adaf5efdc0ce7f22f03686923499e98aaf7f7b456c93e374e6d/${SRCNAME}.tar.xz \
        \
        file://gcc8-hack.patch;striplevel=0 \
        file://gcc8-i386-libgomp.patch;striplevel=0 \
        file://gcc8-sparc-config-detection.patch;striplevel=0 \
        file://gcc8-libgomp-omp_h-multilib.patch;striplevel=0 \
        file://gcc8-libtool-no-rpath.patch;striplevel=0 \
        file://gcc8-isl-dl.patch;striplevel=0 \
        file://gcc8-libstdc++-docs.patch;striplevel=0 \
        file://gcc8-no-add-needed.patch;striplevel=0 \
        file://gcc8-foffload-default.patch;striplevel=0 \
        file://gcc8-Wno-format-security.patch;striplevel=0 \
        file://gcc8-rh1512529-aarch64.patch;striplevel=0 \
        file://gcc8-rh1670535.patch;striplevel=0 \
        file://gcc8-libgomp-20190503.patch;striplevel=0 \
        file://gcc8-pr86747.patch;striplevel=0 \
        file://gcc8-libgomp-testsuite.patch;striplevel=0 \
\
        file://gcc8-rh1668903-1.patch;striplevel=0 \
        file://gcc8-rh1668903-2.patch;striplevel=0 \
        file://gcc8-rh1668903-3.patch;striplevel=0 \
\
        "

SRC_URI[sha256sum] = "0dcdcc6edbb0305e75502f2cf89632a6b8975f9b6105789bf6c30c66d15fe213"

#
# Set some default values
#
gcclibdir = "${libdir}/gcc"

target_includedir ?= "${includedir}"
target_libdir ?= "${libdir}"
target_base_libdir ?= "${base_libdir}"
target_prefix ?= "${prefix}"

# We need to ensure that for the shared work directory, the do_patch signatures match
# The real WORKDIR location isn't a dependency for the shared workdir.
src_patches[vardepsexclude] = "WORKDIR"
should_apply[vardepsexclude] += "PN"

FILESEXTRAPATHS =. "${FILE_DIRNAME}/gcc:"


# There is some bug in this: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=55930
CONFIGUREOPTS_remove = "--disable-dependency-tracking"


# Ignore how TARGET_ARCH is computed.
TARGET_ARCH[vardepvalue] = "${TARGET_ARCH}"

LANGUAGES = "c,c++,fortran,lto"

# TODO: --without-isl => --with-isl
# TODO: nvptx

EXTRA_OECONF = "\
    --disable-bootstrap \
    --enable-languages=${LANGUAGES} \
    --enable-shared \
    --enable-threads=posix \
    --enable-checking=release \
    --enable-multilib \
    --with-system-zlib \
    --enable-__cxa_atexit \
    --disable-libunwind-exceptions \
    --enable-gnu-unique-object \
    --enable-linker-build-id \
    --with-gcc-major-version-only \
    --with-linker-hash-style=gnu \
    --enable-plugin \
    --enable-initfini-array \
    --without-isl \
    --disable-libmpx \
    --enable-decimal-float \
    \
    --program-prefix=${TARGET_PREFIX} \
    --without-local-prefix \
    --disable-install-libiberty \
    --enable-poison-system-directories \
    --enable-libitm \
    --with-gxx-include-dir=/not/exist{target_includedir}/c++/${BINV} \
    --with-sysroot=/not/exist \
    --with-build-sysroot=${STAGING_DIR_TARGET} \
"

EXTRA_OECONF_append_x86-64 = "\
    --enable-cet \
    --with-tune=generic \
    --with-arch_32=i686 \
    "

export gcc_cv_collect2_libs = 'none required'
# We need to set gcc_cv_collect2_libs else there is cross-compilation badness
# in the config.log files (which might not get generated until do_compile
# hence being missed by the insane do_configure check).

CPPFLAGS = ""

SYSTEMHEADERS = "${target_includedir}"
SYSTEMLIBS = "${target_base_libdir}/"
SYSTEMLIBS1 = "${target_libdir}/"

do_configure_prepend () {
	# teach gcc to find correct target includedir when checking libc ssp support
	mkdir -p ${B}/gcc
	echo "NATIVE_SYSTEM_HEADER_DIR = ${SYSTEMHEADERS}" > ${B}/gcc/t-oe
	cat ${S}/gcc/defaults.h | grep -v "\#endif.*GCC_DEFAULTS_H" > ${B}/gcc/defaults.h.new
	cat >>${B}/gcc/defaults.h.new <<_EOF
#define NATIVE_SYSTEM_HEADER_DIR "${SYSTEMHEADERS}"
#define STANDARD_STARTFILE_PREFIX_1 "${SYSTEMLIBS}"
#define STANDARD_STARTFILE_PREFIX_2 "${SYSTEMLIBS1}"
#define SYSTEMLIBS_DIR "${SYSTEMLIBS}"
#endif /* ! GCC_DEFAULTS_H */
_EOF
	mv ${B}/gcc/defaults.h.new ${B}/gcc/defaults.h
}

do_configure () {
	# Setup these vars for cross building only
	# ... because foo_FOR_TARGET apparently gets misinterpreted inside the
	# gcc build stuff when the build is producing a cross compiler - i.e.
	# when the 'current' target is the 'host' system, and the host is not
	# the target (because the build is actually making a cross compiler!)
	if [ "${BUILD_SYS}" != "${HOST_SYS}" ]; then
		export CC_FOR_TARGET="${CC}"
		export GCC_FOR_TARGET="${CC}"
		export CXX_FOR_TARGET="${CXX}"
		export AS_FOR_TARGET="${HOST_PREFIX}as"
		export LD_FOR_TARGET="${HOST_PREFIX}ld"
		export NM_FOR_TARGET="${HOST_PREFIX}nm"
		export AR_FOR_TARGET="${HOST_PREFIX}ar"
		export GFORTRAN_FOR_TARGET="gfortran"
		export RANLIB_FOR_TARGET="${HOST_PREFIX}ranlib"
	fi
	export CC_FOR_BUILD="${BUILD_CC}"
	export CXX_FOR_BUILD="${BUILD_CXX}"
	export CFLAGS_FOR_BUILD="${BUILD_CFLAGS}"
	export CPPFLAGS_FOR_BUILD="${BUILD_CPPFLAGS}"
	export CXXFLAGS_FOR_BUILD="${BUILD_CXXFLAGS}"
	export LDFLAGS_FOR_BUILD="${BUILD_LDFLAGS}"
	export CFLAGS_FOR_TARGET="${TARGET_CFLAGS}"
	export CPPFLAGS_FOR_TARGET="${TARGET_CPPFLAGS}"
	export CXXFLAGS_FOR_TARGET="${TARGET_CXXFLAGS}"
	export LDFLAGS_FOR_TARGET="${TARGET_LDFLAGS}"


	oe_runconf
}

ARCH_FLAGS_FOR_TARGET += "-isystem${STAGING_DIR_TARGET}${target_includedir}"

do_configure_prepend () {
	install -d ${RECIPE_SYSROOT}${target_includedir}
	touch ${RECIPE_SYSROOT}${target_includedir}/limits.h
}

do_compile () {
	export CC="${BUILD_CC}"
	export AR_FOR_TARGET="${TARGET_SYS}-ar"
	export RANLIB_FOR_TARGET="${TARGET_SYS}-ranlib"
	export LD_FOR_TARGET="${TARGET_SYS}-ld"
	export NM_FOR_TARGET="${TARGET_SYS}-nm"
	export CC_FOR_TARGET="${CCACHE} ${TARGET_SYS}-gcc"
	export CFLAGS_FOR_TARGET="${TARGET_CFLAGS}"
	export CPPFLAGS_FOR_TARGET="${TARGET_CPPFLAGS}"
	export CXXFLAGS_FOR_TARGET="${TARGET_CXXFLAGS}"
	export LDFLAGS_FOR_TARGET="${TARGET_LDFLAGS}"

	# Prevent native/host sysroot path from being used in configargs.h header,
	# as it will be rewritten when used by other sysroots preventing support
	# for gcc plugins
	oe_runmake configure-gcc
	sed -i 's@${STAGING_DIR_TARGET}@/host@g' ${B}/gcc/configargs.h
	sed -i 's@${STAGING_DIR_HOST}@/host@g' ${B}/gcc/configargs.h

	# Prevent sysroot/workdir paths from being used in checksum-options.
	# checksum-options is used to generate a checksum which is embedded into
	# the output binary.
	oe_runmake TARGET-gcc=checksum-options all-gcc
	sed -i 's@${DEBUG_PREFIX_MAP}@@g' ${B}/gcc/checksum-options
	sed -i 's@${STAGING_DIR_HOST}@/host@g' ${B}/gcc/checksum-options

	oe_runmake all
        #all-host configure-target-libgcc
	(cd ${B}/${TARGET_SYS}/libgcc; oe_runmake enable-execute-stack.c unwind.h md-unwind-support.h sfp-machine.h gthr-default.h)
}

INHIBIT_PACKAGE_STRIP = "1"

# Compute how to get from libexecdir to bindir in python (easier than shell)
BINRELPATH = "${@os.path.relpath(d.expand("${STAGING_DIR_NATIVE}${prefix_native}/bin/${TARGET_SYS}"), d.expand("${libexecdir}/gcc/${TARGET_SYS}/${BINV}"))}"

ELFARCH_x86_64 = "elf64-x86-64"
ELFARCH_aarch64 = "elf64-littleaarch64"

do_install () {
	( cd ${B}/${TARGET_SYS}/libgcc; oe_runmake 'DESTDIR=${D}' install-unwind_h-forbuild install-unwind_h )
	oe_runmake 'DESTDIR=${D}' install

	install -d ${D}${target_base_libdir}
	install -d ${D}${target_libdir}

        # This is taken from gcc.spec:
        echo '/* GNU ld script
Use the shared library, but some functions are only in
the static library, so try that secondarily.  */
OUTPUT_FORMAT('${ELFARCH}')
GROUP ( libgcc_s.so.1 libgcc.a )' > ${D}/${libdir}/gcc/${TARGET_SYS}/${BINV}/libgcc_s.so

      # Remove gcc python and gdb integration that is in gcc-runtime-native
      rm -rf ${D}${datadir}/gcc-8
      rm -rf ${D}${datadir}/gdb

	# Insert symlinks into libexec so when tools without a prefix are searched for, the correct ones are
	# found. These need to be relative paths so they work in different locations.
	dest=${D}${libexecdir}/gcc/${TARGET_SYS}/${BINV}/
	install -d $dest
	for t in ar as ld ld.bfd ld.gold nm objcopy objdump ranlib strip gcc cpp $fortsymlinks; do
		ln -sf ${BINRELPATH}/${TARGET_PREFIX}$t $dest$t
		ln -sf ${BINRELPATH}/${TARGET_PREFIX}$t ${dest}${TARGET_PREFIX}$t
	done

	# Remove things we don't need but keep share/java
	for d in info man share/doc share/locale share/man share/info; do
		rm -rf ${D}${STAGING_DIR_NATIVE}${prefix_native}/$d
	done

	# libquadmath headers need to  be available in the gcc libexec dir
	install -d ${D}${libdir}/gcc/${TARGET_SYS}/${BINV}/include/
	cp ${S}/libquadmath/quadmath.h ${D}${libdir}/gcc/${TARGET_SYS}/${BINV}/include/
	cp ${S}/libquadmath/quadmath_weak.h ${D}${libdir}/gcc/${TARGET_SYS}/${BINV}/include/

	find ${D}${libdir}/gcc/${TARGET_SYS}/${BINV}/include-fixed -type f -not -name "README" -not -name limits.h -not -name syslimits.h | xargs rm -f
}

do_package[noexec] = "1"
do_packagedata[noexec] = "1"
do_package_write_ipk[noexec] = "1"
do_package_write_rpm[noexec] = "1"
do_package_write_deb[noexec] = "1"

inherit chrpath
