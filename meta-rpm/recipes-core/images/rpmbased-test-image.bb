SUMMARY = "RPM-based test image"

LICENSE = "MIT"

inherit rpm-image

IMAGE_INSTALL = "abattis-cantarell-fonts zenity dropbear"
