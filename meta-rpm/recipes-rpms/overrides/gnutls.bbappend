post_rpm_install_append() {
  # This conflicts with the file from gnutls-libs, so remove
  ls -l ${D}${libdir}
  rm -rf ${D}${libdir}/.libgnutls.so.*
}
